# gazebo_workshop
## Project structure
### Models
Models folder contains all the Gazebo models we created with the Gazebo model/building editor. For this specific case:

```
└── models
    └── building
    └──robot

```

### Scripts
Scripts folder contains all the code that will be executed in this simulation. For this specific case it is not required to have a `catkin_ws`, since there is no interaction with ROS; however, there is a simple plugin that will print a Welcome message in the console when the simulation is launched.

```
└── scripts
    └── hello_world.cpp
```

### World
World folder contains the Gazebo worlds, wich are made up with the previous created models.
```
└── world
    └── new_world.world
```

## Build
To build the script which prints the Welcome message follow the next steps:

1. Create the build folder:
    ```
    mkdir build && cd build
    ```
2. Build the code:
    ```
    cmake ../
    make
    ```
3. Add the build folder to the Gazebo plugin path:
    ```
    export GAZEBO_PLUGIN_PATH=${GAZEBO_PLUGIN_PATH}:$PATH_TO_BUILD_FOLDER
    ```
For example, if running in the container `export GAZEBO_PLUGIN_PATH=${GAZEBO_PLUGIN_PATH}:/home/gazebo-project/build`

## Launch the simulation
1. In the Repository project folder, rum the following command to launch the simulation:
    ```
    gazebo world/new_world
    ```